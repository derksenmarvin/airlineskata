﻿using AirlinesKata.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AirlinesKata.State
{
    public class Flights
    {
        private List<Flight> values = new List<Flight>();

        public Flights Add(Flight value)
        {
            values.Add(value);
            return this;
        }

        public Flights Update(Flight update)
        {
            int index = values.FindIndex(flight => flight.Id == update.Id);
            values[index] = update;
            return this;
        }

        public IEnumerable<Flight> GetAll() => values;

        public IEnumerable<Flight> GetBookable(DateTime now) =>
            values.Where(flight => flight.StateAt(now) == FlightState.Booking);
    }
}
