﻿namespace AirlinesKata.State
{
    public class Account
    {
        public decimal Current { get; private set; } = 0;

        public Account Add(decimal value)
        {
            Current += value;
            return this;
        }
    }
}
