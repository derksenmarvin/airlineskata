﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AirlinesKata
{
    public static class Legend
    {
        private static CursorPosition Position => new(0, Console.WindowHeight - 1);

        public static void Draw(IDictionary<ConsoleKey, string> hotkeys)
        {
            Clear();
            EnsurePosition();
            Console.Write(string.Join("   ", hotkeys.Select(kv => $"[Ctrl + {kv.Key}] {kv.Value}")));
        }

        public static void Clear()
        {
            EnsurePosition();
            Console.Write(new string(' ', Console.WindowWidth));
        }

        private static void EnsurePosition()
        {
            Console.SetCursorPosition(Position.Left, Position.Top);
        }
    }
}
