﻿using System;
using System.Globalization;

namespace AirlinesKata
{
    internal static class Header
    {
        private static readonly CursorPosition cursorState = new(0, 0);
        private static readonly CursorPosition cursorDate = new(3, 0);

        public static void Draw(bool playing, DateTime currentDate, decimal account)
        {
            CursorPosition previousPosition = new(Console.CursorLeft, Console.CursorTop);
            DrawPlaystate(playing);
            DrawDate(currentDate);
            DrawAccount(account);
            Console.WriteLine();
            Console.SetCursorPosition(previousPosition.Left, previousPosition.Top);
        }

        private static void DrawPlaystate(bool playing)
        {
            Console.SetCursorPosition(cursorState.Left, cursorState.Top);
            Console.Write(playing ? "|>" : "||");
        }

        private static void DrawDate(DateTime dateTime)
        {
            Console.SetCursorPosition(cursorDate.Left, cursorDate.Top);
            Console.Write(dateTime.ToString("g"));
        }

        private static void DrawAccount(decimal account)
        {
            CursorPosition frameStart = new(Console.WindowWidth - 30, 0);
            Console.SetCursorPosition(frameStart.Left, frameStart.Top);
            Console.Write($"Balance: EUR {account.ToString("C2", CultureInfo.CurrentCulture)}".PadLeft(30, ' '));
        }
    }
}
