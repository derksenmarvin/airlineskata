﻿using System;
using System.Threading.Tasks;
using System.Timers;

namespace AirlinesKata
{
    public class GameState
    {
        private static readonly DateTime startDate = new DateTime(2020, 08, 24, 12, 0, 0);

        private readonly Timer timer;
        private int tick = 0;

        public GameState(Func<GameState, Task> onTick)
        {
            timer = new Timer(1000);
            timer.AutoReset = true;
            timer.Elapsed += async (sender, e) =>
            {
                tick++;
                await onTick(this);
            };
        }

        public bool NowPlaying { get; private set; } = false;

        public void TogglePlaying()
        {
            NowPlaying = !NowPlaying;

            if (NowPlaying)
            {
                timer.Start();
            }
            else
            {
                timer.Stop();
            }
        }

        public DateTime Now => startDate.AddHours(tick);
    }
}
