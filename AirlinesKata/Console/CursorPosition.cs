﻿namespace AirlinesKata
{
    public record CursorPosition(int Left, int Top) { }
}