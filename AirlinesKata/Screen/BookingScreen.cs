﻿using AirlinesKata.Domain;
using AirlinesKata.State;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AirlinesKata
{
    public class BookingScreen : IInputScreen, ITransitioningScreen
    {
        private readonly GameState gameState;
        private readonly Flights flights;
        private readonly Account account;
        private readonly List<IPrompt> prompts = new List<IPrompt>();

        public BookingScreen(
            GameState gameState,
            Flights flights,
            Account account)
        {
            this.gameState = gameState;
            this.flights = flights;
            this.account = account;
            prompts.Add(new SelectFlightPrompt(flights.GetBookable(gameState.Now)));
        }

        public bool DrawAfterInput => true;

        public event ScreenTransitionEventHandler Transition;

        public IEnumerable<string> Draw()
        {
            yield return "book tickets for flight";
            yield return "";

            foreach (var prompt in prompts)
            {
                foreach (var messageLine in prompt.Message)
                {
                    yield return messageLine;
                }

                if (prompt is IStatefulPrompt s)
                {
                    if (s.Done && s.PrintState)
                    {
                        yield return $"> {s.State}";
                    }
                }
            }
        }

        public void TakeInput(string input)
        {
            IInputPrompt currentInput = prompts
                .OfType<IInputPrompt>()
                .Where(prompt => (prompt is IStatefulPrompt s && !s.Done) || prompt is ICompletingPrompt<BookingResult>)
                .FirstOrDefault();
            bool? success = currentInput?.TakeInput(input);

            if (success.HasValue)
            {
                if (success.Value)
                {
                    prompts.RemoveAll(p => p is InvalidInputPrompt);

                    if (currentInput is ITransitioningPrompt t)
                    {
                        prompts.Add(t.Next);
                    }
                    if (currentInput is ICompletingPrompt<BookingResult> c)
                    {
                        /*
                         * TICKETS GEBUCHT, STATES AKTUALISIEREN
                         */
                        BookingResult result = c.Complete(gameState);
                        flights.Update(result.Flight);
                        account.Add(result.Revenue);
                        Transition?.Invoke(this, typeof(MainScreen));
                    }
                }
                else
                {
                    prompts.Add(new InvalidInputPrompt(input));
                }
            }
        }

        public void TakeHotkey(ConsoleKeyInfo consoleKeyInfo)
        {
            if (consoleKeyInfo.Key == ConsoleKey.Escape)
            {
                Transition?.Invoke(this, typeof(MainScreen));
            }
        }
    }

    internal class SelectFlightPrompt : IStatefulPrompt, ITransitioningPrompt
    {
        private readonly IEnumerable<Flight> bookableFlights;

        public SelectFlightPrompt(IEnumerable<Flight> bookableFlights)
        {
            this.bookableFlights = bookableFlights;
        }

        public IEnumerable<string> Message
        {
            get
            {
                yield return "Enter flight id to book tickets for";
                yield return "";

                foreach (Flight flight in bookableFlights)
                {
                    yield return $"ID: {flight.Id,-10}";
                    yield return $"current booking state: B ({flight.TicketsSoldBusinessClass}/{flight.SeatsBusinessClass})  E ({flight.TicketsSoldEconomyClass}/{flight.SeatsEconomyClass})";
                    yield return new string('-', 10);
                }
            }
        }

        public bool Done => State != null;
        public bool PrintState => true;
        public string State { get; private set; }
        public IPrompt Next => new SelectClassPrompt(bookableFlights, State);

        public bool TakeInput(string input)
        {
            string normalized = input.Trim();
            if (bookableFlights.Any(flight => flight.Id == normalized))
            {
                State = normalized;
                return true;
            }

            return false;
        }
    }

    internal class SelectClassPrompt : IStatefulPrompt, ITransitioningPrompt
    {
        private readonly IEnumerable<Flight> bookableFlights;
        private readonly string flightId;

        public SelectClassPrompt(IEnumerable<Flight> bookableFlights, string flightId)
        {
            this.bookableFlights = bookableFlights;
            this.flightId = flightId;
        }

        public IEnumerable<string> Message => new[] { "Select class to book tickets for [B,E]" };

        public bool Done => State != null;
        public bool PrintState => true;
        public string State { get; private set; }
        public IPrompt Next => new EnterAmountPrompt(bookableFlights, flightId, State);

        public bool TakeInput(string input)
        {
            string normalized = input.Trim().ToUpper();
            if (normalized == "B" || normalized == "E")
            {
                State = normalized;
                return true;
            }

            return false;
        }
    }

    internal class EnterAmountPrompt : IStatefulPrompt, ITransitioningPrompt
    {
        private readonly IEnumerable<Flight> bookableFlights;
        private readonly string flightId;
        private readonly string classIdentifier;
        private readonly int maxAmount;
        private int? amount;

        public EnterAmountPrompt(IEnumerable<Flight> bookableFlights, string flightId, string classIdentifier)
        {
            this.bookableFlights = bookableFlights;
            this.flightId = flightId;
            this.classIdentifier = classIdentifier;

            Flight flight = bookableFlights.First(flight => flight.Id == flightId);
            maxAmount = classIdentifier.ToUpper() switch
            {
                "B" => flight.SeatsBusinessClass - flight.TicketsSoldBusinessClass,
                "E" => flight.SeatsEconomyClass - flight.TicketsSoldEconomyClass,
                _ => 0
            };
        }

        public IEnumerable<string> Message => new[] { $"Provide amount of tickets to book ({maxAmount} max)" };

        public bool Done => amount != null;
        public bool PrintState => true;
        public string State { get => amount.ToString(); }
        public IPrompt Next => new BookingSummaryPrompt(bookableFlights, flightId, classIdentifier, amount.Value);

        public bool TakeInput(string input)
        {
            if (int.TryParse(input.Trim(), out int normalized))
            {
                if (normalized > maxAmount)
                {
                    return false;
                }

                amount = normalized;
                return true;
            }

            return false;
        }
    }

    internal class BookingSummaryPrompt : IInputPrompt, ICompletingPrompt<BookingResult>
    {
        private readonly IEnumerable<Flight> bookableFlights;
        private readonly string flightId;
        private readonly string classIdentifier;
        private readonly int amount;

        public BookingSummaryPrompt(
            IEnumerable<Flight> bookableFlights,
            string flightId,
            string classIdentifier,
            int amount)
        {
            this.bookableFlights = bookableFlights;
            this.flightId = flightId;
            this.classIdentifier = classIdentifier;
            this.amount = amount;
        }

        public IEnumerable<string> Message => new[] { $"Book {amount} tickets (class: {classIdentifier}) for flight {flightId}, ok? [Y]" };

        public BookingResult Complete(GameState gameState)
        {
            Flight flight = bookableFlights.First(flight => flight.Id == flightId);

            return classIdentifier.ToUpper() switch
            {
                "B" => new(flight.BookBusinessClass(amount), amount * 100),
                "E" => new(flight.BookEconomyClass(amount), amount * 50),
                _ => new(flight, 0)
            };
        }

        public bool TakeInput(string input) =>
            input.ToLower() == "y";
    }

    internal record BookingResult(
        Flight Flight,
        decimal Revenue)
    { }
}
