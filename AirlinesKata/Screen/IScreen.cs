﻿using System;
using System.Collections.Generic;

namespace AirlinesKata
{
    public interface IScreen
    {
        bool DrawOnTick { get => false; }

        IEnumerable<string> Draw();

    }

    public interface IHotkeyScreen : IScreen
    {
        IDictionary<ConsoleKey, string> Hotkeys { get; }

        void TakeHotkey(ConsoleKey consoleKey);
    }

    public interface IInputScreen : IScreen
    {
        bool DrawAfterInput { get => false; }

        void TakeInput(string input);
    }

    public interface ITransitioningScreen : IScreen
    {
        event ScreenTransitionEventHandler Transition;
    }

    public delegate void ScreenTransitionEventHandler(IScreen sender, Type next);
}