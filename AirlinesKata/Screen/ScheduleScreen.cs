﻿using AirlinesKata.Domain;
using AirlinesKata.State;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AirlinesKata
{
    public class ScheduleScreen : IInputScreen, ITransitioningScreen
    {
        private readonly GameState gameState;
        private readonly Flights flights;
        private readonly List<IPrompt> schedulePrompts = new List<IPrompt>
        {
            new FromSchedulePrompt()
        };

        public ScheduleScreen(
            GameState gameState,
            Flights flights)
        {
            this.gameState = gameState;
            this.flights = flights;
        }

        public bool DrawAfterInput => true;

        public event ScreenTransitionEventHandler Transition;

        public IEnumerable<string> Draw()
        {
            yield return "schedule new flight";
            yield return "";

            foreach (var prompt in schedulePrompts)
            {
                foreach (var messageLine in prompt.Message)
                {
                    yield return messageLine;
                }

                if (prompt is IStatefulPrompt i)
                {
                    if (i.Done && i.PrintState)
                    {
                        yield return $"> {i.State}";
                    }
                }
            }
        }

        public void TakeInput(string input)
        {
            IInputPrompt currentInput = schedulePrompts
                .OfType<IInputPrompt>()
                .Where(prompt => (prompt is IStatefulPrompt s && !s.Done) || prompt is ICompletingPrompt<Flight>)
                .FirstOrDefault();
            bool? success = currentInput?.TakeInput(input);

            if (success.HasValue)
            {
                if (success.Value)
                {
                    schedulePrompts.RemoveAll(p => p is InvalidInputPrompt);

                    if (currentInput is ITransitioningPrompt t)
                    {
                        schedulePrompts.Add(t.Next);
                    }
                    if (currentInput is ICompletingPrompt<Flight> c)
                    {
                        /*
                         * FLUG GEPLANT, IN STATE SPEICHERN
                         */
                        flights.Add(c.Complete(gameState));
                        Transition?.Invoke(this, typeof(MainScreen));
                    }
                }
                else
                {
                    schedulePrompts.Add(new InvalidInputPrompt(input));
                }
            }
        }

        public void TakeHotkey(ConsoleKeyInfo consoleKeyInfo)
        {
            if (consoleKeyInfo.Key == ConsoleKey.Escape)
            {
                Transition?.Invoke(this, typeof(MainScreen));
            }
        }
    }

    internal class FromSchedulePrompt : IStatefulPrompt, ITransitioningPrompt
    {
        public IEnumerable<string> Message => new[] { $"Enter DEPARTURE AIRPORT, pick from [{string.Join(", ", Airports.All)}]" };
        public bool Done => State != null;
        public bool PrintState => true;
        public string State { get; private set; }
        public IPrompt Next => new ToSchedulePrompt(State);

        public bool TakeInput(string input)
        {
            string normalized = input.Trim().ToUpper();
            if (Airports.All.Contains(normalized))
            {
                State = normalized;
                return true;
            }

            return false;
        }
    }

    internal class ToSchedulePrompt : IStatefulPrompt, ITransitioningPrompt
    {
        private readonly IEnumerable<string> validAirports;
        private readonly string fromAirport;

        public ToSchedulePrompt(string fromAirport)
        {
            validAirports = Airports.Without(fromAirport);
            this.fromAirport = fromAirport;
        }

        public IEnumerable<string> Message => new[] { $"Enter ARRIVAL AIRPORT, pick from [{string.Join(", ", validAirports)}]" };
        public bool Done => State != null;
        public bool PrintState => true;
        public string State { get; private set; }
        public IPrompt Next => new SummarySchedulePrompt(fromAirport, State);

        public bool TakeInput(string input)
        {
            string normalized = input.Trim().ToUpper();
            if (validAirports.Contains(normalized))
            {
                State = normalized;
                return true;
            }

            return false;
        }
    }

    internal class SummarySchedulePrompt : ICompletingPrompt<Flight>
    {
        private readonly string fromAirport;
        private readonly string toAirport;

        public SummarySchedulePrompt(string fromAirport, string toAirport)
        {
            this.fromAirport = fromAirport;
            this.toAirport = toAirport;
        }

        public IEnumerable<string> Message => new[] { $"Schedule flight from {fromAirport} to {toAirport}, ok? [Y]" };

        public Flight Complete(GameState gameState) => new Flight(
            string.Format("{0:X}", Guid.NewGuid().GetHashCode()),
            new Airport(fromAirport),
            new Airport(toAirport),
            gameState.Now.AddHours(24),
            new Aircraft(1000, new MaintenancePlan(800)),
            50,
            200);

        public bool TakeInput(string input) =>
            input.ToLower() == "y";
    }
}
