﻿using System.Collections.Generic;

namespace AirlinesKata
{
    internal interface IPrompt
    {
        IEnumerable<string> Message { get; }
    }

    internal interface IInputPrompt : IPrompt
    {
        bool TakeInput(string input);
    }

    internal interface IStatefulPrompt : IInputPrompt
    {
        bool Done { get; }
        bool PrintState { get; }
        string State { get; }
    }

    internal interface ITransitioningPrompt : IPrompt
    {
        IPrompt Next { get; }
    }

    internal interface ICompletingPrompt<TResult> : IInputPrompt
    {
        TResult Complete(GameState gameState);
    }

    internal class InvalidInputPrompt : IPrompt
    {
        private readonly string input;

        public InvalidInputPrompt(string input)
        {
            this.input = input;
        }

        public IEnumerable<string> Message => new[] { $"> {input}: Invalid input, try again" };
    }
}
