﻿using AirlinesKata.Domain;
using AirlinesKata.State;
using System;
using System.Collections.Generic;

namespace AirlinesKata
{
    public class MainScreen : IHotkeyScreen, ITransitioningScreen
    {
        private static readonly IDictionary<ConsoleKey, string> hotkeys = new Dictionary<ConsoleKey, string>
        {
            { ConsoleKey.P, "Play/Pause" },
            { ConsoleKey.S, "Schedule Flight" },
            { ConsoleKey.B, "Book Tickets" }
        };

        private readonly GameState gameState;
        private readonly Flights flights;

        public MainScreen(
            GameState gameState,
            Flights flights)
        {
            this.gameState = gameState;
            this.flights = flights;
        }

        public IDictionary<ConsoleKey, string> Hotkeys => hotkeys;

        public bool DrawOnTick => true;

        public event ScreenTransitionEventHandler Transition;

        public IEnumerable<string> Draw()
        {
            return WriteFlightsTable(flights.GetAll(), gameState.Now);
        }

        public void TakeHotkey(ConsoleKey key)
        {
            switch (key)
            {
                case ConsoleKey.P: gameState.TogglePlaying(); break;
                case ConsoleKey.S: Transition?.Invoke(this, typeof(ScheduleScreen)); break;
                case ConsoleKey.B: Transition?.Invoke(this, typeof(BookingScreen)); break;
            }
        }

        private static string[] WriteFlightsTable(IEnumerable<Flight> flights, DateTime now)
        {
            var lines = new List<string>()
            {
                FlightsTableHeader,
                FlightsTableDivider
            };

            foreach (var flight in flights)
            {
                lines.Add(FlightsTableRow(flight, now));
            }

            return lines.ToArray();
        }

        private static string FlightsTableRow(Flight flight, DateTime now) =>
            string.Join("|", new[]
            {
                flight.Id.PadRight(10, ' '),
                flight.From.ICAO,
                flight.To.ICAO,
                flight.Departure.ToString("g"),
                flight.ArrivalAt.ToString("g"),
                FlightStateColumn(flight, now),
                ""
            });

        private static string FlightStateColumn(Flight flight, DateTime now) =>
            flight.StateAt(now) switch
            {
                FlightState.Booking => FlightBookingProgress(flight, now),
                FlightState.Ready => $"{"Ready",-20}",
                FlightState.Boarding => $"{"Boarding",-20}",
                FlightState.Departed => FlightProgressBar(flight, now),
                FlightState.Arrived => $"{"Arrived",-20}",
                FlightState.Completed => $"{"Completed",-20}",
                _ => ""
            };

        private static string FlightProgressBar(Flight flight, DateTime now)
        {
            float progress = flight.ProgressAt(now);
            int fullChars = (int)Math.Ceiling((20 - 1) * progress);
            return $"{new string('=', fullChars)}>".PadRight(20, ' ');
        }

        private static string FlightBookingProgress(Flight flight, DateTime now)
        {
            return $"Booking ({flight.TicketsSoldTotal}/{flight.SeatsTotal})".PadRight(20, ' ');
        }

        private static string FlightsTableHeader =>
            string.Join("|", new[]
            {
                $"{"Flight No",-10}",
                $"{"From",-4}",
                $"{"To",-4}",
                $"{"Departure",-16}",
                $"{"Est. Arrival",-16}",
                $"{"Progress",-20}",
                ""
            });

        private static string FlightsTableDivider =>
            string.Join("|", new[]
            {
                new string('-', 10),
                new string('-', 4),
                new string('-', 4),
                new string('-', 16),
                new string('-', 16),
                new string('-', 20),
                ""
            });
    }
}
