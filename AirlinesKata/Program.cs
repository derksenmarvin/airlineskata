﻿using AirlinesKata.State;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AirlinesKata
{
    public enum Hotkey
    {
        None,
        Abort,
        PlayPause,
        ScheduleFlight
    }

    class Program
    {
        private static ServiceProvider serviceProvider;

        private static GameState gameState;
        private static Account account;
        private static IScreen currentScreen;

        static void Main(string[] args)
        {
            Console.TreatControlCAsInput = true;

            serviceProvider = new ServiceCollection()
                .AddSingleton(_ => new GameState(state => OnTick(state)))
                .AddSingleton<Account>()
                .AddSingleton<Flights>()
                .AddTransient<MainScreen>()
                .AddTransient<ScheduleScreen>()
                .AddTransient<BookingScreen>()
                .BuildServiceProvider();

            gameState = serviceProvider.GetRequiredService<GameState>();
            account = serviceProvider.GetRequiredService<Account>();

            Transition(typeof(MainScreen));

            string input = "";
            int curIndex = 0;
            do
            {
                Header.Draw(gameState.NowPlaying, gameState.Now, account.Current);
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                bool withCtrl = (keyInfo.Modifiers & ConsoleModifiers.Control) != 0;

                if (keyInfo.Key == ConsoleKey.Backspace)
                {
                    if (curIndex > 0)
                    {
                        Console.Write(keyInfo.KeyChar);
                        Console.Write(' ');
                        Console.Write(keyInfo.KeyChar);
                        input = input.Length > 0 ? input.Remove(Math.Max(input.Length - 1, 0)) : "";
                        curIndex--;
                    }
                }

                if (currentScreen is IHotkeyScreen h)
                {
                    if (withCtrl && h.Hotkeys.ContainsKey(keyInfo.Key))
                    {
                        h.TakeHotkey(keyInfo.Key);
                        input = "";
                        curIndex = 0;
                    }
                }

                if (currentScreen is IInputScreen i)
                {
                    if (keyInfo.Key == ConsoleKey.Enter)
                    {
                        i.TakeInput(input);
                        input = "";
                        curIndex = 0;

                        if (i == currentScreen && i.DrawAfterInput)
                        {
                            DrawScreen(i);
                        }
                    }
                }

                if (currentScreen is IInputScreen && !withCtrl && keyInfo.Key != ConsoleKey.Enter && keyInfo.Key != ConsoleKey.Backspace)
                {
                    input += keyInfo.KeyChar;
                    Console.Write(keyInfo.KeyChar);
                    curIndex++;
                }
            }
            while (true);
        }

        private static Task OnTick(GameState state)
        {
            Header.Draw(state.NowPlaying, state.Now, account.Current);

            if (currentScreen.DrawOnTick)
            {
                DrawScreen(currentScreen);
            }

            return Task.CompletedTask;
        }

        private static void Transition(Type next)
        {
            currentScreen = (IScreen)serviceProvider.GetRequiredService(next);

            BindScreenEvents(currentScreen);
            DrawScreen(currentScreen);
        }

        private static void BindScreenEvents(IScreen screen)
        {
            if (screen is ITransitioningScreen t)
            {
                t.Transition += (_, next) => Transition(next);
            }
        }

        private static void DrawScreen(IScreen screen)
        {
            ClearScreen();

            if (currentScreen is IHotkeyScreen h)
            {
                Legend.Draw(h.Hotkeys);
            }
            else
            {
                Legend.Clear();
            }

            Console.SetCursorPosition(0, 2);
            IEnumerable<string> lines = screen.Draw();
            foreach (var line in lines)
            {
                Console.WriteLine(line);
            }
            Console.SetCursorPosition(0, lines.Count() + 2);

            if (currentScreen is IInputScreen)
            {
                Console.Write("> ");
            }
        }

        private static void ClearScreen()
        {
            int lineCount = Console.WindowHeight - 2 - 2;

            for (int line = 2; line < lineCount; line++)
            {
                Console.SetCursorPosition(0, line);
                Console.Write(new string(' ', Console.WindowWidth));
            }
        }
    }
}
