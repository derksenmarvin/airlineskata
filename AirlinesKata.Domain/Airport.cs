﻿using System.Collections.Generic;
using System.Linq;

namespace AirlinesKata.Domain
{
    public record Airport(string ICAO) { }

    public static class Airports
    {
        public static IEnumerable<string> All => new[] { "EDDK", "LTFM", "YMML", "KJFK" };

        public static IEnumerable<string> Without(string exclude) =>
            All.Where(a => a != exclude);
    }
}