﻿namespace AirlinesKata.Domain
{
    public record Aircraft(
        int KilometersPerHour,
        MaintenancePlan MaintenancePlan)
    { }
}