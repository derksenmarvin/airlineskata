﻿namespace AirlinesKata.Domain
{
    public record MaintenancePlan(decimal Costs)
    { }
}