﻿using System.Collections.Generic;

namespace AirlinesKata.Domain
{
    public static class Distance
    {
        private static readonly IDictionary<(Airport From, Airport To), int> kilometers =
            new Dictionary<(Airport From, Airport To), int>()
            {
                { (new Airport("EDDK"), new Airport("LTFM")), 2000 },
                { (new Airport("EDDK"), new Airport("YMML")), 16000 },
                { (new Airport("LTFM"), new Airport("YMML")), 14000 },
                { (new Airport("EDDK"), new Airport("KJFK")), 6000 },
                { (new Airport("KJFK"), new Airport("LTFM")), 8000 },
                { (new Airport("KJFK"), new Airport("YMML")), 17000 },
            };

        public static int Between(Airport from, Airport to)
        {
            bool hasResult1 = kilometers.TryGetValue((from, to), out int result1);
            bool hasResult2 = kilometers.TryGetValue((to, from), out int result2);

            return hasResult1 ? result1 : hasResult2 ? result2 : throw new System.Exception();
        }
    }
}