﻿using System;

namespace AirlinesKata.Domain
{
    public enum FlightState
    {
        Booking,
        Ready,
        Boarding,
        Departed,
        Arrived,
        Completed
    }

    public record Flight(
        string Id,
        Airport From,
        Airport To,
        DateTime Departure,
        Aircraft Aircraft,
        int SeatsBusinessClass,
        int SeatsEconomyClass,
        int TicketsSoldBusinessClass = 0,
        int TicketsSoldEconomyClass = 0)
    {
        public int SeatsTotal =>
            SeatsBusinessClass + SeatsEconomyClass;

        public DateTime ArrivalAt =>
            Departure.AddHours(FlightDurationHours);

        public int TicketsSoldTotal =>
            TicketsSoldBusinessClass + TicketsSoldEconomyClass;

        private DateTime BoardingAt =>
            Departure.AddHours(-1);

        private int FlightDurationHours =>
            Distance.Between(From, To) / Aircraft.KilometersPerHour;

        private bool Booked =>
               TicketsSoldBusinessClass == SeatsBusinessClass
            && TicketsSoldEconomyClass == SeatsEconomyClass;

        public Flight BookBusinessClass(int amount) =>
            this with { TicketsSoldBusinessClass = TicketsSoldBusinessClass + amount };

        public Flight BookEconomyClass(int amount) =>
            this with { TicketsSoldEconomyClass = TicketsSoldEconomyClass + amount };


        public FlightState StateAt(DateTime now)
        {
            if (now > ArrivalAt) return FlightState.Completed;
            if (now == ArrivalAt) return FlightState.Arrived;
            if (now >= Departure) return FlightState.Departed;
            if (now == BoardingAt) return FlightState.Boarding;
            if (now < BoardingAt && Booked) return FlightState.Ready;

            return FlightState.Booking;
        }

        public float ProgressAt(DateTime now) =>
            (now - Departure).Hours / (float)FlightDurationHours;
    }
}
